#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void folder_create(){
    pid_t child_id;
    int status;

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        char *argv[] = {"mkdir", "-p", "quote", NULL};
        execv("/bin/mkdir", argv);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        char *argv[] = {"mkdir", "-p", "music", NULL};
        execv("/bin/mkdir", argv);
    }
}

void unzip(){
    pid_t child_id;
    int status;

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        char *argv[] = {"unzip", "quote.zip", "-d", "/quote", NULL};
        execv("/bin/unzip", argv);
    } else {
        while ((wait(&status)) > 0);
        char *argv[] = {"unzip", "music.zip", "-d", "/music", NULL};
        execv("/bin/unzip", argv);
    }
}

int main(){
    pid_t child_id;
    int status;

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        folder_create();
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        unzip();
    }
}
